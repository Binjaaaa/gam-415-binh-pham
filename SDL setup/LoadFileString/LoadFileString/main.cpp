#include <iostream>
#include <fstream>
#include <string>
#include <exception>
using namespace std;

string LoadFileString(const string& filename)
{
	string result = "";
	string line;
	ifstream file;

	file.open(filename);

	if (!file.is_open())
	{
		throw runtime_error("LoadFileString() failed to open file.");
	}

	if (file.is_open())
	{
		while (getline(file, line))
		{
			result += line + "\n";
		}
		file.close();
	}

	return result;
}

int main(int argc, char* argv[])
{
	//LoadFileString("ReadThis.txt");

	try
	{
		string s = LoadFileString("ReadThis.txt");
		cout << "Loaded: " << endl << s << endl;
	}
	catch (exception& e)
	{
		cout << "Exception! " << e.what() << endl;
	}

	cin.get();
	cin.get();


	return 0;
}