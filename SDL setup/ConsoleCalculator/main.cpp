#include <iostream>
using namespace std;

struct Calculator
{
	float Add(float first, float second)
	{
		float result;
		result = first + second;

		return result;
	}

	float Subtract(float first, float second)
	{
		float result;
		result = first - second;

		return result;
	}

	float Multiply(float first, float second)
	{
		float result;
		result = first * second;

		return result;
	}

	float Divide(float first, float second)
	{
		float result;
		result = first / second;

		return result;
	}
};

int main(int argc, char* argv[])
{
	// TODO: Implement a calculator in the middle

	// Organize into a class
	// Seperate functionality for each operation +, -, *, /
	// Need to interpret second input as an operator
	// Something like float.TryParse("string")
	Calculator calc;

	float num1;
	float num2;
	string operation;
	float finalResult;

	cout << "Please type in a number and press enter." << endl;
	cin >> num1;

	cout << "Please type in an operation you wish to perform then press enter." << endl;
	cin >> operation;

	cout << "Please type in another number and press enter." << endl;
	cin >> num2;

	if (operation == "+")
	{
		finalResult = calc.Add(num1, num2);
	}
	else if (operation == "-")
	{
		finalResult = calc.Subtract(num1, num2);
	}
	else if (operation == "*")
	{
		finalResult = calc.Multiply(num1, num2);
	}
	else if (operation == "/")
	{
		finalResult = calc.Divide(num1, num2);
	}

	cout << "Really, you needed me to tell you that " << num1 << " " << operation << " " << num2 << " = " << finalResult<< "?" << endl;
	cout << "... What a dummy!" << endl;

	cin.get();
	cin.get();


	return 0;
}
