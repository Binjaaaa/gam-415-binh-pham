#pragma once
#include <string>
#include <vector>
#include <functional>

#ifdef BUILD_LIBRARY
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif

/*
Game Engine parts
-----------------
Input handling
	Device states, Events
	SDL_GetKeyboardState(), SDL_PollEvent()
Rendering / graphics
Helpful containers, math vectors, functionality, QUATERNIONS
Collisions / physics
Geometry editing/creation
Audio
AI / nav meshes
Networking
Scripting language
UI
Databases
I18n (internationalization)


*/

DLL_EXPORT void PrintAThing();

DLL_EXPORT std::string RoundFloat(double input, int decimalPlaces);

DLL_EXPORT void ArrayToSpreadsheet(uint32_t width, uint32_t height, std::string** data, std::string path);

DLL_EXPORT std::string ChangeCase(std::string aString);


// Scancodes, technically.  Let's keep it simple.  We can use SDL_SCANCODE_TO_KEYCODE to convert, if needed.  See SDL_scancode.h for more values.
enum class KeyCode
{
	None,
	A = 4,
	D = 7,
	S = 22,
	W = 26,
	Y = 28,
	Return = 40,
	Enter = 40,
	Escape = 41,
	Space = 44,
	Right = 79,
	Left = 80,
	Down = 81,
	Up = 82,
};

enum class KeyModifier
{
	None = 0x0000,
	LShift = 0x0001,
	RShift = 0x0002,
	LCtrl = 0x0040,
	RCtrl = 0x0080,
	LAlt = 0x0100,
	RAlt = 0x0200,
	LGui = 0x0400,
	RGui = 0x0800,
	NumLock = 0x1000,
	CapsLock = 0x2000,
	Mode = 0x4000,
	Reserved = 0x8000
};

// Event class holding callbacks that take no arguments and return nothing
class DLL_EXPORT SimpleEvent
{
private:
	std::vector<std::function<void()>>* callbacks;

public:
	SimpleEvent()
	{
		callbacks = new std::vector<std::function<void()>>();
	}
	~SimpleEvent()
	{
		delete callbacks;
	}

	SimpleEvent& operator+=(std::function<void()> fn)
	{
		callbacks->push_back(fn);
		return *this;
	}

	void Clear()
	{
		callbacks->clear();
	}

	void Invoke()
	{
		for (auto fn : *callbacks)
		{
			fn();
		}
	}
};


// Event class holding callbacks that take a KeyCode and a KeyModifier
class DLL_EXPORT KeyEvent
{
private:
	std::vector<std::function<void(KeyCode, KeyModifier)>>* callbacks;

public:
	KeyEvent()
	{
		callbacks = new std::vector<std::function<void(KeyCode, KeyModifier)>>();
	}
	~KeyEvent()
	{
		delete callbacks;
	}

	KeyEvent& operator+=(std::function<void(KeyCode, KeyModifier)> fn)
	{
		callbacks->push_back(fn);
		return *this;
	}

	void Clear()
	{
		callbacks->clear();
	}

	void Invoke(KeyCode key, KeyModifier mod)
	{
		for (auto fn : *callbacks)
		{
			fn(key, mod);
		}
	}
};


// Class container for static methods to set up and control the game engine
class DLL_EXPORT GameEngine
{
public:
	static SimpleEvent OnQuit;
	static KeyEvent OnKeyDown;
	static KeyEvent OnKeyUp;
	static SimpleEvent OnUpdate;

	static bool Init(const std::string& title, unsigned int width, unsigned int height);
	static void Quit();

	static void Run();
	static void Start();
	static void Awake();

	static void Deinit();

private:

	static bool done;
};


class DLL_EXPORT Input
{
public:

	static bool GetKey(KeyCode key);
};


