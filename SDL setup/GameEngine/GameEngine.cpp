#include "GameEngine.h"
#include <iostream>
#include <string>
#include <fstream>
#include "SDL.h"
using namespace std;


// GameEngine static member variables
SimpleEvent GameEngine::OnQuit;
KeyEvent GameEngine::OnKeyDown;
KeyEvent GameEngine::OnKeyUp;
SimpleEvent GameEngine::OnUpdate;
bool GameEngine::done;



void PrintAThing()
{
	cout << "A Thing" << endl;
}


string RoundFloat(double input, int decimalPlaces)
{
	string returnMe = "";
	string fullString = to_string(input);
	string decimal = ".";

	returnMe.append(fullString.substr(0, fullString.find(decimal)));
	fullString.erase(0, fullString.find(decimal) + 1);
	returnMe.append(decimal);

	for (int i = 0; i < decimalPlaces; i++)
	{
		char temp = fullString.at(i);
		returnMe.append(1, temp);
	}

	return returnMe;
}


void ArrayToSpreadsheet(uint32_t width, uint32_t height, string** data, string path)
{
	std::ofstream outFile(path);
	for (uint32_t y = 0; y < height; y++)
	{
		for (uint32_t x = 0; x < width; ++x)
		{
			outFile << data[x][y] << ",";
		}
		outFile << "\n";
	}
	outFile.close();
}

string ChangeCase(string aString)
{
	string returnString = "";
	for (auto s : aString)
	{
		char tempChar;
		if (s > 64 && s < 91)
		{
			tempChar = char((int)s + 32);
		}
		else if (s > 96 && s < 123)
		{
			tempChar = char((int)s - 32);
		}
		else
		{
			tempChar = s;
		}
		returnString += tempChar;
	}
	return returnString;
}


bool GameEngine::Init(const std::string& title, unsigned int width, unsigned int height)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		return false;

	SDL_Window* window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		width, height, SDL_WINDOW_OPENGL);

	if (window == nullptr)
		return false;

	return true;
}

void GameEngine::Deinit()
{
	SDL_Quit();
}


void GameEngine::Quit()
{
	done = true;
}

void GameEngine::Awake()
{

}

void GameEngine::Start()
{

}

void GameEngine::Run()
{
	// TODO: Call Awake()
	//Awake();
	// TODO: Call Start()
	//Start();
	// Main loop
	done = false;
	SDL_Event event;
	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				done = true;
				OnQuit.Invoke();
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_F4 && (event.key.keysym.mod & KMOD_ALT))
				{
					done = true;
				}
				OnKeyDown.Invoke(static_cast<KeyCode>(event.key.keysym.scancode), static_cast<KeyModifier>(event.key.keysym.mod));
			}
			else if (event.type == SDL_KEYUP)
			{
				OnKeyUp.Invoke(static_cast<KeyCode>(event.key.keysym.scancode), static_cast<KeyModifier>(event.key.keysym.mod));
			}
		}

		OnUpdate.Invoke();
	}
}


bool Input::GetKey(KeyCode key)
{
	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	if (key != KeyCode::None)
		return keystates[(int)key];

	return false;
}

