#version 330 core
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 inTexCoords;

out vec2 texCoords;

uniform float time;

void main()
{
	gl_Position = vec4(vertex.x , vertex.y, vertex.z, 1.0);
	texCoords = inTexCoords;
}