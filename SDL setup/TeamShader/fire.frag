#version 330 core
in vec2 texCoords;
out vec4 outColor;

uniform sampler2D noiseTex;
uniform sampler2D noiseTex2;
uniform float time;

void main()
{
    // set up our scroll rates for the different textures and axes
    vec2 noiseScrollX = vec2(time * 0.23, time * 0.1);
    vec2 noiseScrollY = vec2(time * -0.13, time * 0.11);
    vec2 noise2Scroll = vec2(time * 0.13, time * -0.19);

    // create distorted tex coords using the noise texture
    vec2 noiseTexCoords = vec2(texture2D(noiseTex, texCoords + noiseScrollX).r, texture2D(noiseTex, texCoords + noiseScrollY + vec2(0.5, 0.5)).r);

    // sample texture at distorted coords
	vec4 texColor = step(clamp(0.5 - texCoords.y, 0, 0.5), texture2D(noiseTex2, noiseTexCoords + noise2Scroll));

    // fade out color as it rises
    vec4 fadeColor = mix(vec4(1, 1, 1, 1), vec4(0, 0, 0, 0), clamp(texCoords.y + 0.2, 0, 1));

    // because who needs more variables?
    vec4 fireColor = vec4(0.99, 0.65, 0.01, 1);

	outColor = texColor * fadeColor * fireColor;
}