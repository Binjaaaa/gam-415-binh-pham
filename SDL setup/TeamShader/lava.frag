#version 330 core
in vec2 texCoords;
out vec4 outColor;

uniform sampler2D noiseTex;
uniform sampler2D albedoTex;
uniform float time;

void main()
{
    // set up our scroll rates for the different textures and axes
    vec2 noiseScrollX = vec2(time * 0.1, time * 0.23);
    vec2 noiseScrollY = vec2(time * -0.11, time * 0.13);
    vec2 albedoScroll = vec2(time * -0.17, time * 0.13);

    // create distorted tex coords using the noise texture
    vec2 noiseTexCoords = vec2(texture2D(noiseTex, texCoords + noiseScrollX).r, texture2D(noiseTex, texCoords + noiseScrollY + vec2(0.5, 0.5)).r);

    // sample texture at distorted coords
	vec4 texColor = texture2D(albedoTex, noiseTexCoords + albedoScroll);

	outColor = texColor;
}