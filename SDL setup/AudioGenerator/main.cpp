#include <iostream>
#include <functional>
#include "SDL_gpu.h"
#include "AudioBuffer.h"
using namespace std;


// See SampleGenerators.cpp
Sint16 GetSineSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume);
Sint16 GetSquareSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume);
Sint16 GetTriangleSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume);
Sint16 GetSawtoothSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume);
Sint16 GetNoiseSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume);


// Put an audio sample into the audio buffer using a specified generator function
void MixSample(std::function<Sint16(AudioBuffer&, int, float, Sint16)> sampleFn, AudioBuffer& buffer, float frequency, Sint16 volume)
{
	// Make a variable that tells us how far along the wave we have traveled
	int sampleIndex = buffer.GetSampleStartingIndex();

	for (int i = 0; i < buffer.GetSize(); i += 2)
	{
		Sint16 sample = sampleFn(buffer, sampleIndex, frequency, volume);
		buffer.MixMonoSample(sampleIndex, sample);
		++sampleIndex;
	}
}

// Check if the user is holding down keys.  Make that affect the audio.
void HandleAudioInput(AudioBuffer& buffer)
{
	const Uint8* keystates = SDL_GetKeyboardState(nullptr);
	static float freq = 261.626f;

	if (keystates[SDL_SCANCODE_EQUALS])
	{
		freq *= 1.2f;
		GPU_LogError("Freq: %.2f\n", freq);
	}
	if (keystates[SDL_SCANCODE_MINUS])
	{
		freq /= 1.2f;
		GPU_LogError("Freq: %.2f\n", freq);
	}

	// Play a tone
	if (keystates[SDL_SCANCODE_1])
		MixSample(GetSineSample, buffer, freq, 3000);

	if (keystates[SDL_SCANCODE_2])
		MixSample(GetSquareSample, buffer, freq, 3000);

	if (keystates[SDL_SCANCODE_3])
		MixSample(GetTriangleSample, buffer, freq, 3000);

	if (keystates[SDL_SCANCODE_4])
		MixSample(GetSawtoothSample, buffer, freq, 3000);

	if (keystates[SDL_SCANCODE_5])
		MixSample(GetNoiseSample, buffer, freq, 3000);

	
	/*int t = buffer.GetSampleStartingIndex();
	if (t > 100000 && t < 200000)
	{
		MixSample(GetSineSample, buffer, freq, 3000);
	}*/

}


// Fill the audio buffer with sound samples.  This is called directly by the SDL audio subsystem.
// Assumes bufferLength matches our buffer size
void AudioGeneratorCallback(void* userdata, Uint8* stream, int bufferLength)
{
	AudioBuffer* buffer = static_cast<AudioBuffer*>(userdata);
	buffer->Lock();

	/*if (silence)
	{
		memset(stream, 0, bufferLength);
		return;
	}*/

	buffer->Clear();
	HandleAudioInput(*buffer);  // Let the user press some keys to change what happens in the audio thread


	stream += buffer->CopyIntoStream(stream);

	buffer->Step();
	buffer->Unlock();
}


Sint16 maxAmplitude = 0;

// Visualization of a wave/line
void DrawWaveform(GPU_Target* screen, AudioBuffer& buffer, GPU_Rect area)
{
	static Sint16 lastValue = 0;
	float xStep = area.w / (buffer.GetSize() / 2);
	float x = 0.0f;
	float y = area.y + area.h / 2;

	maxAmplitude = 0;

	// Lock audio thread so we can read from a complete buffer instead of during the audio callback
	SDL_LockAudio();
	buffer.Lock();

	// Sometimes it seems that read samples are messed up, but SDL_LockAudio() isn't fixing it...
	for (int i = 0; i < buffer.GetSize(); i += 2)
	{
		Sint16 value = buffer.GetSample(i);  // only the left channel
		float y0 = y - lastValue / 100.0f;
		float y1 = y - value / 100.0f;

		GPU_Line(screen, x, y0, x + xStep, y1, GPU_MakeColor(0, 0, 255, 255));
		if (x < 400 && x + xStep >= 400)
			maxAmplitude = value;

		x += xStep;
		lastValue = value;
	}

	buffer.Unlock();
	SDL_UnlockAudio();

}

Uint32 MyTimerCallback(Uint32 interval, void* params)
{
	int* someData = (int*)params;
	cout << "Your data was " << *someData << endl;

	return 0;
}

int main(int argc, char* argv[])
{
	// Set up the window
	GPU_Target* screen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);
	if (screen == nullptr)
		return 1;

	SDL_SetWindowTitle(SDL_GetWindowFromID(screen->context->windowID), "My Audio Test");

	SDL_InitSubSystem(SDL_INIT_TIMER);

	int* someData = new int();
	*someData = 12;

	SDL_AddTimer(1000, MyTimerCallback, someData);

	// Set up audio stuff
	AudioBuffer* buffer = new AudioBuffer();
	buffer->Init(48000, 4096, AudioGeneratorCallback);





	GPU_Image* smileImage = GPU_LoadImage("smile.png");
	if (smileImage == nullptr)
		return 2;

	// Array of boolean values: Is a key held down or not?
	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	float x = 300;
	float y = 300;
	float radius = 100.0f;

	float degrees = 0;

	float dt = 0.010f;

	// Main loop
	SDL_Event event;
	bool done = false;
	while (!done)
	{
		// Handle input events
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
			}
		}


		// Draw stuff
		GPU_ClearRGB(screen, 255, 255, 255);

		DrawWaveform(screen, *buffer, GPU_MakeRect(0, 0, screen->w, screen->h));

		// Visualization of a sprite riding at the max amplitude of the waveform
		GPU_Rect dest = { x - radius, y - radius - maxAmplitude / 100, 2 * radius, 2 * radius };
		GPU_BlitRectX(smileImage, nullptr, screen, &dest, degrees, smileImage->w / 2.0f, smileImage->h / 2.0f, GPU_FLIP_NONE);

		GPU_Flip(screen);

		SDL_Delay(1);
	}

	GPU_FreeImage(smileImage);

	// Deinit audio stuff
	delete buffer;

	GPU_Quit();

	return 0;
}