#include "SDL.h"
#include <functional>
#include "AudioBuffer.h"
using namespace std;


Sint16 GetSineSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// Figure out how the sample rate relates to real time
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	// Tells us how far along the wave we have traveled
	float waveSegment = sampleIndex / float(samplesPerPeriod);

	return volume * sin(2 * M_PI * waveSegment);
}

int sign(float value)
{
	return (value >= 0.0) ? 1 : -1;
}

Sint16 GetSquareSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// Figure out how the sample rate relates to real time
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	// Tells us how far along the wave we have traveled
	float waveSegment = sampleIndex / float(samplesPerPeriod);

	return volume * sign(sin(2 * M_PI * waveSegment));
}

Sint16 GetTriangleSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// Figure out how the sample rate relates to real time
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	// Tells us how far along the wave we have traveled
	float waveSegment = sampleIndex / float(samplesPerPeriod);

	return ((2 * volume)/M_PI) * asin(sin(2 * M_PI * waveSegment));
}

int cot(float value)
{
	return (cos(value) / sin(value));
}

Sint16 GetSawtoothSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	float period = 1 / frequency;
	// Figure out how the sample rate relates to real time
	int samplesPerPeriod = buffer.GetSampleRate() / frequency;

	// Tells us how far along the wave we have traveled
	float waveSegment = sampleIndex / float(samplesPerPeriod);

	return -((2 * volume)/M_PI) * atan(1/(tan((waveSegment * M_PI))));
}

Sint16 GetNoiseSample(AudioBuffer& buffer, int sampleIndex, float frequency, Sint16 volume)
{
	// TODO: Implement me!
	
	// Not sure what kind of noise this was... is it random noise or something like white noise?

	return 0;
}