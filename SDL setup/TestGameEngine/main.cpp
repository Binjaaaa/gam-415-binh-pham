#include <iostream>
#include "GameEngine.h"
#include <functional>
#include <vector>
#include <algorithm>
using namespace std;


class Entity
{
private:

	int id;
	string name;
	string tag;

	// TODO: Contain a list of Components

public:
	// TODO: Add accessors for those private members
};

class Component
{
public:
	int id;

	virtual void Awake()
	{

	}
	virtual void Start()
	{

	}
	virtual void Update()
	{

	}
};


class PlayerController : public Component
{
public:
	void Update() override
	{
		if (Input::GetKey(KeyCode::W))
		{
			cout << "You are holding the W key." << endl;
		}
		else
		{
			cout << "You are NOT holding the W key." << endl;
		}
	}
};


int Print5(int a, int b)
{
	cout << 5 << endl;
	return 0;
}

void DoSomethingWithTwoIntegers(std::function<int(int, int)> fn, int a, int b)
{
	if (fn != nullptr)
		fn(a, b);
}


// Register with OnKeyDown
void QuitWhenYouPressEscape(KeyCode key, KeyModifier mod)
{
	if (key == KeyCode::Escape)
	{
		GameEngine::Quit();
	}
}

void PrintKeyDown(KeyCode key, KeyModifier mod)
{
	cout << "You pressed: " << (int)key << endl;
}

void PrintKeyUp(KeyCode key, KeyModifier mod)
{
	cout << "You released: " << (int)key << endl;
}


void RunScene()
{

	GameEngine::OnQuit += []() {
		cout << "Goodbye!" << endl;
	};

	GameEngine::OnKeyDown += PrintKeyDown;
	GameEngine::OnKeyUp += PrintKeyUp;
	GameEngine::OnKeyDown += QuitWhenYouPressEscape;



	// TODO: Implement GameObject::Instantiate() to create a new GameObject, initialize it, and register its callbacks.
	// TODO: Do we want a Transform component in every GameObject by default?
	/*GameObject* obj = GameObject::Instantiate();
	obj->AddComponent<PlayerController>();
*/
	PlayerController player;

	vector<Component*> components;
	components.push_back(&player);

	for (auto obj : components)
	{
		GameEngine::OnUpdate += [obj]() {obj->Update(); };
	}



	// Start the main loop
	GameEngine::Run();

	GameEngine::OnQuit.Clear();
	GameEngine::OnKeyDown.Clear();
	GameEngine::OnUpdate.Clear();
	GameEngine::OnKeyUp.Clear();
}


int main(int argc, char* argv[])
{
	GameEngine::Init("Welcome to Game.", 1280, 720);



	RunScene();


	// Clean up
	GameEngine::Deinit();

	return 0;
}