#include <iostream>
#include "SDL.h"
#include "glew.h"
#include "SDL_Image.h"
#include <fstream>
#include <string>
#include <chrono>
#include <thread>
#include <vector>
using namespace std;

// Returns the entire file loaded into a single string
string LoadFileString(const string& filename)
{
	string result = "";
	string line;
	ifstream inFile;

	inFile.open(filename);

	if (!inFile.is_open())
	{
		throw runtime_error("LoadFileString() failed to open file.");
	}

	if (inFile.is_open())
	{
		while (getline(inFile, line))
		{
			result += line + "\n";
		}
		inFile.close();
	}
	return result;
}

float RandomFloat(float min, float max)
{
	return min + (rand() % 1001) / 1000.0f * (max - min);
}

bool CompileShader(const string & shaderName, GLuint shader)
{
	// Compile shader
	glCompileShader(shader);

	// What happened in that compile step?
	int compileStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);

	if (compileStatus != GL_TRUE)
	{
		// Something went wrong!
		// How many bytes in the error message?
		int logLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		// What is the error message?
		char* shaderMessage = new char[logLength + 1];
		glGetShaderInfoLog(shader, logLength, nullptr, shaderMessage);

		cout << "Failed to compile " << shaderName << " shader: " << shaderMessage << endl;
		delete[] shaderMessage;
		return false;
	}
	return true;
}

GLuint LoadShaders(std::string vertShaderName, std::string fragShaderName)
{
	// Load files into strings
	string vertexString = LoadFileString(vertShaderName);
	string fragmentString = LoadFileString(fragShaderName);

	// Allocate shaders in GL driver
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Give GL the source to the shader
	const GLchar* vertexSource = vertexString.c_str();
	glShaderSource(vertexShader, 1, &vertexSource, nullptr);

	const GLchar* fragmentSource = fragmentString.c_str();
	glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);


	bool vertexResult = CompileShader("Vertex", vertexShader);
	bool fragmentResult = CompileShader("Fragment", fragmentShader);

	// Set up a shader program
	GLuint program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	int linkStatus;
	glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
	if (linkStatus != GL_TRUE)
	{
		int logLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		char* shaderMessage = new char[logLength + 1];
		glGetProgramInfoLog(program, logLength, nullptr, shaderMessage);

		cout << "Failed to link shader program: " << shaderMessage << endl;
		delete[] shaderMessage;
	}

	//glUseProgram(program);

	// TODO: Free the shaders!
	return program;
}

void FreeShaders()
{
	// glDeleteShader(vertexShader);
}

int main(int argc, char* argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
	{
		cout << "SDL failed to initialize!" << SDL_GetError << endl;
		return 1;
	}

    IMG_Init(IMG_INIT_JPG);

	// Create window
	SDL_Window* window = SDL_CreateWindow("GAM 415", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 512, SDL_WINDOW_OPENGL);
	if (window == nullptr)
	{
		cout << "Failed to create window! " << SDL_GetError() << endl;
		return 2;
	}

	// Tell SDL which OpenGL version to use
	// Version 3.3 has all of the features we need, but this may need to change if we want
	// to enforce support for later shading language versions.
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GLContext glContext = SDL_GL_CreateContext(window);

	if (glContext == nullptr)
	{
		cout << "Failed to set up OpenGL context: " << SDL_GetError() << endl;
		return 3;
	}

	GLenum glewStatus = glewInit();
	if (glewStatus != GLEW_OK)
	{
		cout << "Failed to initialize GLEW." << endl;
		return 4;
	}

	const GLubyte* versionString = glGetString(GL_VERSION);

	cout << "OpenGL version string: " << versionString << endl;


	//load the lava and fire shaders
	GLuint lavaShaderProgram = LoadShaders("lava.vert", "lava.frag");
	GLuint fireShaderProgram = LoadShaders("fire.vert", "fire.frag");


	// Bind vertex array object (container for reusable geometry and its properties)
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Bind a vertex buffer object to store out geometry data on the GPU
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);


	vector<float> square1VertexBuffer = {
		//vertices written in this form: (x, y, z, u, v)

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Square 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		//tri 1
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, //vert 1 
		-1.0f, 1.0f, 0.0f, 0.0f, 1.0f, //vert 2
		0.0f, 1.0f, 0.0f, 1.0f, 1.0f, //vert 3
		//tri 2
		0.0f, 1.0f, 0.0f, 1.0f, 1.0f, //vert 1
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, //vert2
		0.0f, -1.0f, 0.0f, 1.0f, 0.0f  //vert3

	};

	glBufferData(GL_ARRAY_BUFFER, square1VertexBuffer.size() * sizeof(float), &square1VertexBuffer[0], GL_DYNAMIC_DRAW);

	GLuint vbo2;
	glGenBuffers(1, &vbo2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo2); 

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Square 2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	vector<float> square2VertexBuffer = {
		//tri 1
		0.0f, -1.0f, 0.0f, 0.0f, 0.0f, //vert 1
		0.0f, 1.0f, 0.0f, 0.0f, 1.0f, //vert 2
		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  // vert

		// tri 2
		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  //vert 1
		0.0f, -1.0f, 0.0f, 0.0f, 0.0f, //vert 2
		1.0f, -1.0f, 0.0f, 1.0f, 0.0f //vert 3
	};

	glBufferData(GL_ARRAY_BUFFER, square2VertexBuffer.size() * sizeof(float), &square2VertexBuffer[0], GL_DYNAMIC_DRAW);
	int stride = 5;


	//Load up the lava texture
	SDL_Surface* lavaSurface = IMG_Load("lavatex.jpg");

	//load the fire texture
	SDL_Surface* fireSurface = IMG_Load("firenoisetex.jpg");
	
	//load the noise texture
	SDL_Surface* noiseSurface = IMG_Load("noisetex.jpg");

	GLuint textures[3];

	glGenTextures(3, textures);
	
	glActiveTexture(GL_TEXTURE0);

	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, lavaSurface->w, lavaSurface->h, 0, GL_RGB, GL_UNSIGNED_BYTE, lavaSurface->pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glActiveTexture(GL_TEXTURE1);

	glBindTexture(GL_TEXTURE_2D, textures[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, noiseSurface->w, noiseSurface->h, 0, GL_RGB, GL_UNSIGNED_BYTE, noiseSurface->pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glActiveTexture(GL_TEXTURE2);

	glBindTexture(GL_TEXTURE_2D, textures[2]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, fireSurface->w, fireSurface->h, 0, GL_RGB, GL_UNSIGNED_BYTE, fireSurface->pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	SDL_FreeSurface(fireSurface);
	fireSurface = nullptr;

	SDL_FreeSurface(noiseSurface);
	noiseSurface = nullptr;

	SDL_FreeSurface(lavaSurface);
	lavaSurface = nullptr;

	glUniform1i(glGetUniformLocation(lavaShaderProgram, "noiseTex"), 1);
	glUniform1i(glGetUniformLocation(lavaShaderProgram, "albedoTex"), 0);

	glUniform1i(glGetUniformLocation(fireShaderProgram, "noiseTex"), 1);
	glUniform1i(glGetUniformLocation(fireShaderProgram, "noiseTex2"), 2);
	

	//get the timed locations
	int lavaTimeLocation = glGetUniformLocation(lavaShaderProgram, "time");
	int fireTimeLocation = glGetUniformLocation(fireShaderProgram, "time");


	// Handle some input
	bool done = false;
	SDL_Event event;
	const Uint8* keyboardState = SDL_GetKeyboardState(nullptr);

	GLint n = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &n);

	float interval = 3.0f;

	double timeCount = 0;

	clock_t thisTime = clock();
	clock_t lastTime = thisTime;

	bool aBool = false;

	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				done = true;
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					done = true;
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_W)
				{
					// move up?
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_RETURN) // Homework 3
				{
					cout << "OpenGL version string: " << versionString << endl;
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_SPACE)
				{
					for (GLint i = 0; i < n; i++)
					{
						const char* extension =
							(const char*)glGetStringi(GL_EXTENSIONS, i);
						printf("Ext %d: %s\n", i, extension);
					}
				}

			}
		}

		// Do some rendering.
		glClearColor(0.8f, 0.0f, 0.4f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glUseProgram(lavaShaderProgram);
		glUniform1f(lavaTimeLocation, SDL_GetTicks() / 1000.0f);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(
			0, // Shader attribute location
			3, // Number of elements per vertex
			GL_FLOAT, // Data type 
			GL_FALSE, // Should OpenGL normalize this attribute?
			stride * sizeof(float), // Stride / extra spacing between elements
			(void*)0 // Offset from the beginning of the buffer
		);
		// UV Coords
		glVertexAttribPointer(
			1, // Shader attribute location
			2, // Number of elements per vertex
			GL_FLOAT, // Data type 
			GL_FALSE, // Should OpenGL normalize this attribute?
			stride * sizeof(float), // Stride / extra spacing between elements
			(void*)(3 * sizeof(float)) // Offset from the beginning of the buffer
		);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);
		

		glBindBuffer(GL_ARRAY_BUFFER, vbo2);
		glUseProgram(fireShaderProgram);
		glUniform1f(fireTimeLocation, SDL_GetTicks() / 1000.0f);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(
			0, // Shader attribute location
			3, // Number of elements per vertex
			GL_FLOAT, // Data type 
			GL_FALSE, // Should OpenGL normalize this attribute?
			stride * sizeof(float), // Stride / extra spacing between elements
			(void*)0 // Offset from the beginning of the buffer
		);
		// UV Coords
		glVertexAttribPointer(
			1, // Shader attribute location
			2, // Number of elements per vertex
			GL_FLOAT, // Data type 
			GL_FALSE, // Should OpenGL normalize this attribute?
			stride * sizeof(float), // Stride / extra spacing between elements
			(void*)(3 * sizeof(float)) // Offset from the beginning of the buffer
		);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);

		SDL_GL_SwapWindow(window);

		SDL_Delay(1);
	}
	cin.get();
	cin.get();


	return 0;
}
