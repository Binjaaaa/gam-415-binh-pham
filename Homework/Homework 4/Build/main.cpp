#include <iostream>
#include "SDL.h"
#include "glew.h"
//#include "Random.h"
#include <fstream>
#include <string>
#include <chrono>
#include <thread>
using namespace std;

// Returns the entire file loaded into a single string
string LoadFileString(const string& filename)
{
	string result = "";
	string line;
	ifstream inFile;

	inFile.open(filename);

	if (!inFile.is_open())
	{
		throw runtime_error("LoadFileString() failed to open file.");
	}

	if (inFile.is_open())
	{
		while (getline(inFile, line))
		{
			result += line + "\n";
		}
		inFile.close();
	}
	return result;
}

float RandomFloat(float min, float max)
{
	return min + (rand() % 1001) / 1000.0f * (max - min);
}

bool CompileShader(const string& shaderName, GLuint shader)
{
	// Compile shader
	glCompileShader(shader);

	// What happened in that compile step?
	int compileStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);

	if (compileStatus != GL_TRUE)
	{
		// Something went wrong!
		// How many bytes in the error message?
		int logLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		// What is the error message?
		char* shaderMessage = new char[logLength + 1];
		glGetShaderInfoLog(shader, logLength, nullptr, shaderMessage);

		cout << "Failed to compile " << shaderName << " shader: " << shaderMessage << endl;
		delete[] shaderMessage;
		return false;
	}
	return true;
}

void LoadShaders()
{
	// Load files into strings
	string vertexString = LoadFileString("basic.vert");
	string fragmentString = LoadFileString("basic.frag");

	// Allocate shaders in GL driver
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Give GL the source to the shader
	const GLchar* vertexSource = vertexString.c_str();
	glShaderSource(vertexShader, 1, &vertexSource, nullptr);

	const GLchar* fragmentSource = fragmentString.c_str();
	glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);


	bool vertexResult = CompileShader("Vertex", vertexShader);
	bool fragmentResult = CompileShader("Fragment", fragmentShader);

	// Set up a shader program
	GLuint program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	int linkStatus;
	glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
	if (linkStatus != GL_TRUE)
	{
		int logLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		char* shaderMessage = new char[logLength + 1];
		glGetProgramInfoLog(program, logLength, nullptr, shaderMessage);

		cout << "Failed to link shader program: " << shaderMessage << endl;
		delete[] shaderMessage;
	}

	glUseProgram(program);

	// TODO: Free the shaders!
}

void FreeShaders()
{
	// glDeleteShader(vertexShader);
}

int main(int argc, char* argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
	{
		cout << "SDL failed to initialize!" << SDL_GetError << endl;
		return 1;
	}

	// Create window
	SDL_Window* window = SDL_CreateWindow("GAM 415", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 768, SDL_WINDOW_OPENGL);
	if (window == nullptr)
	{
		cout << "Failed to create window! " << SDL_GetError() << endl;
		return 2;
	}

	// Tell SDL which OpenGL version to use
	// Version 3.3 has all of the features we need, but this may need to change if we want
	// to enforce support for later shading language versions.
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GLContext glContext = SDL_GL_CreateContext(window);

	if (glContext == nullptr)
	{
		cout << "Failed to set up OpenGL context: " << SDL_GetError() << endl;
		return 3;
	}

	GLenum glewStatus = glewInit();
	if (glewStatus != GLEW_OK)
	{
		cout << "Failed to initialize GLEW." << endl;
		return 4;
	}

	const GLubyte* versionString = glGetString(GL_VERSION);

	cout << "OpenGL version string: " << versionString << endl;

	LoadShaders();

	// Bind vertex array object (container for reusable geometry and its properties)
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Bind a vertex buffer object to store out geometry data on the GPU
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	// Define geometry
	float vertexBuffer[] = {
		0.5f, 1.0f, 0.0f, // top center
		1.0f, 0.0f, 0.0f, // bottom right
		0.0f, 0.0f, 0.0f // bottom left
	};

	// Upload to the GPU
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), vertexBuffer, GL_DYNAMIC_DRAW);




	//Random rng;
	//float someNumber = rng.Range(0.0f, 1.0f);

	// SDL_GetMouseState()
	// SDL_GetKeyboardState()
	// Look em up, use em:

	/*
	Print a message to the console when the left mouse button is beign held down.
	Print a different message when the 'k' key is being held down.
	Print a third different message when both are happening
	*/

	// Handle some input
	bool done = false;
	SDL_Event event;
	const Uint8* keyboardState = SDL_GetKeyboardState(nullptr);

	GLint n = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &n);

	float interval = 3.0f;

	double timeCount = 0;

	clock_t thisTime = clock();
	clock_t lastTime = thisTime;

	bool aBool = false;

	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				done = true;
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					done = true;
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_W)
				{
					// move up?
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_RETURN) // Homework 3
				{
					cout << "OpenGL version string: " << versionString << endl;
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_SPACE)
				{
					for (GLint i = 0; i < n; i++)
					{
						const char* extension =
							(const char*)glGetStringi(GL_EXTENSIONS, i);
						printf("Ext %d: %s\n", i, extension);
					}
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_1)
				{
					glClearColor(0.5f, 0.0f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_2)
				{
					glClearColor(0.0f, 0.5f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_3)
				{
					glClearColor(0.0f, 0.0f, 0.5f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_4)
				{
					glClearColor(0.5f, 0.5f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_5)
				{
					glClearColor(0.5f, 0.0f, 0.5f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}

				if (event.key.keysym.scancode == SDL_SCANCODE_6)
				{
					aBool = true;
					while(aBool)
					{
						if (event.key.keysym.scancode == SDL_SCANCODE_S)
						{
							aBool = false;
						}

						glClearColor(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), 1.0f);
						glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
						SDL_GL_SwapWindow(window);

						this_thread::sleep_for(chrono::seconds(3));
					}
				}
			}
		}

		// Do input device state handling here.



		// Do some rendering.
		//glClearColor(0.4f, 0.0f, 0.4f, 1.0f);
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



		// Submit vertex/geomtry data
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(
			0, // Shader attribute location
			3, // Number of elements per vertex
			GL_FLOAT, // Data type 
			GL_FALSE, // Should OpenGL normalize this attribute?
			3 * sizeof(float), // Stride / extra spacing between elements
			(void*)0 // Offset from the beginning of the buffer
		);

		glDrawArrays(GL_TRIANGLES, 0, 3);

		glDisableVertexAttribArray(0);


		//SDL_GL_SwapWindow(window);

		SDL_Delay(1);
	}
	cin.get();

	return 0;
}