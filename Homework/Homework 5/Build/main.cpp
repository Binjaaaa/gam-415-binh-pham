#include <iostream>
#include "SDL.h"
#include "glew.h"
//#include "Random.h"
#include <fstream>
#include <string>
#include <chrono>
#include <thread>
#include <vector>
using namespace std;

// Returns the entire file loaded into a single string
string LoadFileString(const string& filename)
{
	string result = "";
	string line;
	ifstream inFile;

	inFile.open(filename);

	if (!inFile.is_open())
	{
		throw runtime_error("LoadFileString() failed to open file.");
	}

	if (inFile.is_open())
	{
		while (getline(inFile, line))
		{
			result += line + "\n";
		}
		inFile.close();
	}
	return result;
}

float RandomFloat(float min, float max)
{
	return min + (rand() % 1001) / 1000.0f * (max - min);
}

bool CompileShader(const string& shaderName, GLuint shader)
{
	// Compile shader
	glCompileShader(shader);

	// What happened in that compile step?
	int compileStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);

	if (compileStatus != GL_TRUE)
	{
		// Something went wrong!
		// How many bytes in the error message?
		int logLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		// What is the error message?
		char* shaderMessage = new char[logLength + 1];
		glGetShaderInfoLog(shader, logLength, nullptr, shaderMessage);

		cout << "Failed to compile " << shaderName << " shader: " << shaderMessage << endl;
		delete[] shaderMessage;
		return false;
	}
	return true;
}

GLuint LoadShaders()
{
	// Load files into strings
	string vertexString = LoadFileString("basic.vert");
	string fragmentString = LoadFileString("basic.frag");

	// Allocate shaders in GL driver
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Give GL the source to the shader
	const GLchar* vertexSource = vertexString.c_str();
	glShaderSource(vertexShader, 1, &vertexSource, nullptr);

	const GLchar* fragmentSource = fragmentString.c_str();
	glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);


	bool vertexResult = CompileShader("Vertex", vertexShader);
	bool fragmentResult = CompileShader("Fragment", fragmentShader);

	// Set up a shader program
	GLuint program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	int linkStatus;
	glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
	if (linkStatus != GL_TRUE)
	{
		int logLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		char* shaderMessage = new char[logLength + 1];
		glGetProgramInfoLog(program, logLength, nullptr, shaderMessage);

		cout << "Failed to link shader program: " << shaderMessage << endl;
		delete[] shaderMessage;
	}

	glUseProgram(program);

	// TODO: Free the shaders!
	return program;
}

void FreeShaders()
{
	// glDeleteShader(vertexShader);
}

int main(int argc, char* argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
	{
		cout << "SDL failed to initialize!" << SDL_GetError << endl;
		return 1;
	}

	// Create window
	SDL_Window* window = SDL_CreateWindow("GAM 415", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 768, SDL_WINDOW_OPENGL);
	if (window == nullptr)
	{
		cout << "Failed to create window! " << SDL_GetError() << endl;
		return 2;
	}

	// Tell SDL which OpenGL version to use
	// Version 3.3 has all of the features we need, but this may need to change if we want
	// to enforce support for later shading language versions.
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GLContext glContext = SDL_GL_CreateContext(window);

	if (glContext == nullptr)
	{
		cout << "Failed to set up OpenGL context: " << SDL_GetError() << endl;
		return 3;
	}

	GLenum glewStatus = glewInit();
	if (glewStatus != GLEW_OK)
	{
		cout << "Failed to initialize GLEW." << endl;
		return 4;
	}

	const GLubyte* versionString = glGetString(GL_VERSION);

	cout << "OpenGL version string: " << versionString << endl;

	GLuint shaderProgram = LoadShaders();

	// Bind vertex array object (container for reusable geometry and its properties)
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Bind a vertex buffer object to store out geometry data on the GPU
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	// x, y, z, r, g, b, a
	// x, y, z, r, g, b, a
	// x, y, z, r, g, b, a

	//1.0f, 0.0f, 0.0f, 1.0f, // color Red
	//0.5f, 0.0f, // Texture coords, (u, v) or (s, t)

	//0.0f, 0.0f, 1.0f, 1.0f, // color Green
	//0.0f, 1.0f, // (u, v)

	//0.0f, 1.0f, 0.0f, 1.0f, // color Blue
	//1.0f, 1.0f, // (u, v)
	// Define geometry
	vector<float> vertexBuffer = {
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ B1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		0.0f, 0.0f, 0.0f, // VERT 1
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.0f, 1.0f, 0.0f, // VERT 2
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.6f, 0.4f, 0.0f, // VERT 3
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		//~~~~~~~~~~~~~~~~ B2 ~~~~~~~~~~~~~~~~~~

		0.0f, 1.0f, 0.0f, // VERT 1
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.3f, 1.0f, 0.0f, // VERT 2
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.4f, 0.6f, 0.0f, // VERT 3
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		//~~~~~~~~~~~~~~~~ B3 ~~~~~~~~~~~~~~~~~~

		0.3f, 1.0f, 0.0f, // VERT 1
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.5f, 0.8f, 0.0f, // VERT 2
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.4f, 0.6f, 0.0f, // VERT 3
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		//~~~~~~~~~~~~~~~~ B4 ~~~~~~~~~~~~~~~~~~

		0.5f, 0.8f, 0.0f, // VERT 1
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.5f, 0.7f, 0.0f, // VERT 2
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.4f, 0.6f, 0.0f, // VERT 3
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		//~~~~~~~~~~~~~~~~ B5 ~~~~~~~~~~~~~~~~~~

		0.0f, 0.0f, 0.0f, // VERT 1
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.6f, 0.4f, 0.0f, // VERT 2
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.4f, 0.0f, 0.0f, // VERT 3
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		//~~~~~~~~~~~~~~~~ B6 ~~~~~~~~~~~~~~~~~~

		0.4f, 0.0f, 0.0f, // VERT 1
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.6f, 0.4f, 0.0f, // VERT 2
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		0.6f, 0.2f, 0.0f, // VERT 3
		1.0f, 0.0f, 0.0f, 1.0f, // color Red

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ T1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		0.6f, 0.9f, 0.0f, // VERT 1
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		0.6f, 1.0f, 0.0f, // VERT 2
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		1.0f, 0.9f, 0.0f, // VERT 3
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		//~~~~~~~~~~~~~~~~ T2 ~~~~~~~~~~~~~~~~~~

		0.6f, 1.0f, 0.0f, // VERT 1
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		1.0f, 1.0f, 0.0f, // VERT 2
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		1.0f, 0.9f, 0.0f, // VERT 3
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		//~~~~~~~~~~~~~~~~ T3 ~~~~~~~~~~~~~~~~~~

		0.75f, 0.9f, 0.0f, // VERT 1
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		0.85f, 0.9f, 0.0f, // VERT 2
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		0.85f, 0.5f, 0.0f, // VERT 3
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		//~~~~~~~~~~~~~~~~ T4 ~~~~~~~~~~~~~~~~~~

		0.75f, 0.9f, 0.0f, // VERT 1
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		0.85f, 0.5f, 0.0f, // VERT 2
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		0.75f, 0.5f, 0.0f, // VERT 3
		0.0f, 0.0f, 1.0f, 1.0f, // color Green

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ P1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		0.6f, 0.0f, 0.0f, // VERT 1
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.6f, 0.5f, 0.0f, // VERT 2
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.7f, 0.0f, 0.0f, // VERT 3
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		//~~~~~~~~~~~~~~~~ P2 ~~~~~~~~~~~~~~~~~~

		0.6f, 0.5f, 0.0f, // VERT 1
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.7f, 0.0f, 0.0f, // VERT 2
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.7f, 0.5f, 0.0f, // VERT 3
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		//~~~~~~~~~~~~~~~~ P3 ~~~~~~~~~~~~~~~~~~

		0.7f, 0.5f, 0.0f, // VERT 1
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.9f, 0.2f, 0.0f, // VERT 2
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.7f, 0.2f, 0.0f, // VERT 3
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		//~~~~~~~~~~~~~~~~ P4 ~~~~~~~~~~~~~~~~~~

		0.7f, 0.5f, 0.0f, // VERT 1
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.9f, 0.5f, 0.0f, // VERT 2
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.9f, 0.2f, 0.0f, // VERT 3
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		//~~~~~~~~~~~~~~~~ P5 ~~~~~~~~~~~~~~~~~~

		0.9f, 0.5f, 0.0f, // VERT 1
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		1.0f, 0.4f, 0.0f, // VERT 2
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.9f, 0.2f, 0.0f, // VERT 3
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		//~~~~~~~~~~~~~~~~ P6 ~~~~~~~~~~~~~~~~~~

		1.0f, 0.4f, 0.0f, // VERT 1
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		1.0f, 0.3f, 0.0f, // VERT 2
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue

		0.9f, 0.2f, 0.0f, // VERT 3
		0.0f, 1.0f, 0.0f, 1.0f, // color Blue
	};

	int stride = 7;

	// Upload to the GPU
	//glBufferData(GL_ARRAY_BUFFER, bVertexBuffer.size() * sizeof(float), &bVertexBuffer[0], GL_DYNAMIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER, tVertexBuffer.size() * sizeof(float), &tVertexBuffer[0], GL_DYNAMIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, vertexBuffer.size() * sizeof(float), &vertexBuffer[0], GL_DYNAMIC_DRAW);


	int timeLocation = glGetUniformLocation(shaderProgram, "time");

	/*
	// Load up the texture
	SDL_Surface* smileSurface = SDL_LoadBMP("images/smile.bmp");

	GLuint smileTexture;
	glGenTextures(1, &smileTexture);
	glBindTexture(GL_TEXTURE_2D, smileTexture);// set this texture as the active one
	glEnable(GL_TEXTURE_2D);

	// Send texture data to the GPU
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, smileSurface->w, smileSurface->h, 0, GL_BGR, GL_UNSIGNED_BYTE, smileSurface->pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	SDL_FreeSurface(smileSurface);
	smileSurface = nullptr;
	*/
	// TODO: Free Texture
	// glDeleteTexture(1, &smileTexture);

	//Random rng;
	//float someNumber = rng.Range(0.0f, 1.0f);

	// SDL_GetMouseState()
	// SDL_GetKeyboardState()
	// Look em up, use em:

	/*
	Print a message to the console when the left mouse button is beign held down.
	Print a different message when the 'k' key is being held down.
	Print a third different message when both are happening
	*/

	// Handle some input
	bool done = false;
	SDL_Event event;
	const Uint8* keyboardState = SDL_GetKeyboardState(nullptr);

	GLint n = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &n);

	float interval = 3.0f;

	double timeCount = 0;

	clock_t thisTime = clock();
	clock_t lastTime = thisTime;

	bool aBool = false;

	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				done = true;
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					done = true;
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_W)
				{
					// move up?
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_RETURN) // Homework 3
				{
					cout << "OpenGL version string: " << versionString << endl;
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_SPACE)
				{
					for (GLint i = 0; i < n; i++)
					{
						const char* extension =
							(const char*)glGetStringi(GL_EXTENSIONS, i);
						printf("Ext %d: %s\n", i, extension);
					}
				}
				/* //								Homework #4
				else if (event.key.keysym.scancode == SDL_SCANCODE_1)
				{
					glClearColor(0.5f, 0.0f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_2)
				{
					glClearColor(0.0f, 0.5f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_3)
				{
					glClearColor(0.0f, 0.0f, 0.5f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_4)
				{
					glClearColor(0.5f, 0.5f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}
				else if (event.key.keysym.scancode == SDL_SCANCODE_5)
				{
					glClearColor(0.5f, 0.0f, 0.5f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					SDL_GL_SwapWindow(window);
				}

				if (event.key.keysym.scancode == SDL_SCANCODE_6)
				{
					aBool = true;
					while(aBool)
					{
						if (event.key.keysym.scancode == SDL_SCANCODE_S)
						{
							aBool = false;
						}

						glClearColor(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), 1.0f);
						glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
						SDL_GL_SwapWindow(window);

						this_thread::sleep_for(chrono::seconds(3));
					}
				}*/
			}
		}

		// Do input device state handling here.


		// Do some rendering.
		glClearColor(0.8f, 0.0f, 0.4f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Not so great: moving by changing the geometry
		//vertexBuffer[0] = 0.5f + sin(SDL_GetTicks() / 1000.0f);
		//glBufferData(GL_ARRAY_BUFFER, vertexBuffer.size() * sizeof(float), &vertexBuffer[0], GL_DYNAMIC_DRAW);

		glUniform1f(timeLocation, SDL_GetTicks() / 1000.0f);

		// Submit vertex/geomtry data for interpretation/ rendering
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		//glEnableVertexAttribArray(2);

		// Positions
		glVertexAttribPointer(
			0, // Shader attribute location
			3, // Number of elements per vertex
			GL_FLOAT, // Data type 
			GL_FALSE, // Should OpenGL normalize this attribute?
			stride * sizeof(float), // Stride / extra spacing between elements
			(void*)0 // Offset from the beginning of the buffer
		);
		// Colors
		glVertexAttribPointer(
			1, // Shader attribute location
			4, // Number of elements per vertex
			GL_FLOAT, // Data type 
			GL_FALSE, // Should OpenGL normalize this attribute?
			stride * sizeof(float), // Stride / extra spacing between elements
			(void*)(3 * sizeof(float)) // Offset from the beginning of the buffer
		);
		/*
		// Texture Coordinates
		glVertexAttribPointer(
			2, // Shader attribute location
			2, // Number of elements per vertex
			GL_FLOAT, // Data type
			GL_FALSE, // Should OpenGL normalize this attribute?
			stride * sizeof(float), // Stride / extra spacing between elements
			(void*)(7 * sizeof(float)) // Offset from the beginning of the buffer
		);*/

		glDrawArrays(GL_TRIANGLES, 0, 48);

		//glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);

		SDL_GL_SwapWindow(window);

		SDL_Delay(1);
	}
	cin.get();
	cin.get();


	return 0;
}
